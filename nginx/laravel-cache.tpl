server {
    listen      %ip%:%proxy_port%;
    server_name %domain_idn% %alias_idn%;
    root           %docroot%/public/;
    access_log     /var/log/%web_system%/domains/%domain%.log combined;
    access_log     /var/log/%web_system%/domains/%domain%.bytes bytes;
    error_log  /var/log/%web_system%/domains/%domain%.error.log error;

    location = / {
        try_files /page-cache/pc__index__pc.html @fallback;
    }

    location / {
        try_files $uri $uri/ /page-cache/$uri.html @fallback;
    }

    location ~* ^.+\.(%proxy_extentions%)$ {
        expires        max;
    }

    location /error/ {
        alias   %home%/%user%/web/%domain%/document_errors/;
    }

    location @fallback {
        proxy_pass      http://%ip%:%web_port%;
    }

    location ~ /\.ht    {return 404;}
    location ~ /\.svn/  {return 404;}
    location ~ /\.git/  {return 404;}
    location ~ /\.hg/   {return 404;}
    location ~ /\.bzr/  {return 404;}

    include %home%/%user%/conf/web/nginx.%domain%.conf*;
}